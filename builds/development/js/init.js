(function(){

  var config = {
    viewFactor : 0.15,
    duration   : 800,
    distance   : "0px",
    scale      : 0.8
  };

  window.sr = ScrollReveal( config );

  if (sr.isSupported()) {
    document.documentElement.classList.add('sr');
  }
})();




// $(document).ready(function() {
//   $("[rel='tooktip']").tooltip();

//   $('client-project-item').hover(function(){
//       $(this).find('.placeholder').slideDown(250);
//     },
//       $(this).find('.placeholder').slideUp(250);
//   });

// });