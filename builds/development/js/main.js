/**
 * Animated Header
 */
var animateHeader = (function() {

	var getDocElement = document.documentElement, 
		header = document.querySelector('.navbar-default'),
		scrolled = false,
		changeHeaderOn = 150;

	function init() {
		window.addEventListener('scroll', function(event) {
			if(!scrolled) {
				scrolled = true;
				setTimeout(scrollPage, 150);
			}
		}, false);
	}

	function scrollPage() {
		var vertScroll = scrollY();

		if(vertScroll >= changeHeaderOn) {
			classie.add(header, 'navbar-shrink');
		} 
		else {
			classie.remove(header, 'navbar-shrink');
		}
		scrolled = false;
	}

	function scrollY() {
		return window.pageYOffset || getDocElement.scrollTop;
	}

	init();

})(); 

/**
 * JQuery for page scrolling feature (requires JQuery Easing plugin)
 */
$(function() {
	$('a.page-scroll').bind('click', function(event) {
		var $anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $($anchor.attr('href')).offset().top
		}, 1000, 'easeInOutExpo');
		event.preventDefault();
	});
});

/**
 * Highlight top nav as scrolling occurs
 */
$('body').scrollspy({
	target: '.navbar-fixed-top'
});

/**
 * Closes the responsive menu on menu item click
 */






//countTo
////*
    jQuery(document).ready(function( $ ) {
        $('.number').counterUp({
            delay: 1,
            time: 1000
        });
    });

